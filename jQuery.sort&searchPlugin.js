﻿(function( $ ){

	var defaults = {
		sort_by : 0,
		sort_method : "asc",
		search_by : 0,
		search_param : ""	
	};

	var options;

	var methods = {
		
		//сортировка объектов
		sort : function(params) { 
				options = $.extend({}, defaults, options, params);
				var sorted = sortArray(this, options);
				this.html(sorted);
			},
        //поиск объектов
        search : function(params) {
            options = $.extend({}, defaults, options, params);
            search(this, options);
        }		
	};

	$.fn.sort_search = function(method) {


		if ( methods[method] ) {
	      	return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
	    } else if ( typeof method === 'object' || ! method ) {
	      	return methods.sort.apply( this, arguments );
	    } else {
	      	$.error( 'Метод ' +  method + ' в jQuery.plugin не существует' );
	    }
	};
})( jQuery );

//функция сортировки
function sortArray(object, options) {
	var array = object.children().toArray();
	var sortedArray = array;

	for (var i = 0; i < sortedArray.length - 1; i++) {

		for (var j = i + 1; j < sortedArray.length; j++) {

			var temp1 = $(sortedArray[i]).find(options.sort_by).html().toUpperCase();
			var temp2 = $(sortedArray[j]).find(options.sort_by).html().toUpperCase();

			if (temp1 > temp2) {
				var temp = sortedArray[i];
				sortedArray[i] = sortedArray[j];
				sortedArray[j] = temp;
			}

		}

	}

	if (options.sort_method == "asc")
		return array;
	else if (options.sort_method == "desc") return array.reverse();
}

//функция поиска
function search(object, options) {

	//если параметр поиска не пустой
	if (options.search_param != "") {

		var index = options.search_by.indexOf(" ");
		//если есть родитель
		if (index != -1) {

			var parent = options.search_by.substring(0, index);
			object.find(options.search_by).not(":contains('" + options.search_param + "')").parents(parent).hide();
		}
		//если нет родителя
		else object.find(options.search_by).not(":contains('" + options.search_param + "')").hide();

	}
	//если параметр поиска пустой
	else {

		var index = options.search_by.indexOf(" ");

		if (index != -1) {

			var parent = options.search_by.substring(0, index);
			object.find(options.search_by).parents(parent).show();
		}

		else object.find(options.search_by).show();
	}
}